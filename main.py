#!/usr/bin/env python3
import argparse
import time
import subprocess
from sys import argv
import scapy.all as sc
from pprint import pprint

def parse_agrs():
    parser = argparse.ArgumentParser(description='scaner')
    parser.add_argument('--host', dest='host', help='host to scan')
    parser.add_argument('-t', dest='scan_type', help='SYN or TCP or UDP')
    return parser.parse_args(argv[1:])

def is_alive(host):
    icmp = sc.IP(dst=host)/sc.ICMP()
    response = sc.sr1(icmp, timeout=0.5)
    if response == None: return False
    else:
        return True

def chunks(l, n):
    for i in range(0, len(l), n):
        yield l[i:i + n]

class SYN():
    def __init__(self, host, ports):
        self.host = host
        self.ports = ports

    def is_open(self, timeout=0.2):
        results = {port:None for port in self.ports}
        to_reset = []
        p = sc.IP(dst=self.host)/sc.TCP(dport=self.ports, flags='S')
        answers, un_answered = sc.sr(p, timeout=timeout)
        for requests, response in answers:
            if not response.haslayer(sc.TCP):
                continue
            tcp_layer = response.getlayer(sc.TCP)
            if tcp_layer.flags == 0x12:
                to_reset.append(tcp_layer.sport)
                results[tcp_layer.sport] = True
            elif tcp_layer.flags == 0x14:
                results[tcp_layer.sport] = False
        self._reset_half_open(self.host, to_reset)
        return results

    def _reset_half_open(self, host, ports):
        sc.sr(sc.IP(dst=host)/sc.TCP(dport=ports, flags='AR'), timeout=1)

class TCP():
    def __init__(self, host):
        self.host = host

    def scan(self):
        p = subprocess.Popen(['nmap', self.host], stdout=subprocess.PIPE)
        return self._parse(p.stdout.read().decode().split('\n'))

    def _parse(self, output):
        result = '\n'.join([x.replace('open', '').replace('/tcp', '') for x in output[5:-3]])
        return f'{output[1].replace("Nmap scan report for ", "")}\n\
Ports open:\n\
{result}'


class UDP():
    def __init__(self, host, ports):
        self.host = host
        self.ports = ports

    def scan(self):
        results = []
        for port in self.ports:
            p = subprocess.Popen(['nc', '-vnzu', self.host, str(port)], stderr=subprocess.PIPE)
            results.append(p.stderr.read().decode())
        return self._parse(results)

    def _parse(self, output):
        result = ''
        for portline in output:
            if portline.split()[3] != '(?)':
                result += f'{portline.split()[2]}  {portline.split()[3]}\n'
        return f'Ports open:\n{result}'

def main():
    args = parse_agrs()
    if not args.host or not args.scan_type:
        print('no args\n-h to help')
        exit()
    if args.scan_type == 'SYN':
        if is_alive(args.host):
            for ports in chunks(range(1, 1024), 100):
                results = SYN(args.host, ports).is_open()
                for p, r in results.items():
                    print(f'{p}:{r}')
    elif args.scan_type == 'TCP':
        print(TCP(args.host).scan())
    elif args.scan_type == 'UDP':
        if is_alive(args.host):
            for ports in chunks(range(1, 1024), 100):
                results = UDP(args.host, ports).scan()
                print(results)
    else:
        print('wrong scan_type')
        exit()

if __name__ == '__main__':
    main()
