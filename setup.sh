#!/bin/bash
sudo apt install -y nmap
python3 -m venv env
cp ./env/bin/python3 /tmp/
sudo setcap cap_net_raw=eip /tmp/python3
rm ./env/bin/python3
ln -s /tmp/python3 ./env/bin/python3
. ./env/bin/activate
pip3 install scapy
echo 'now activate env by: . ./env/bin/activate'
